<?php


function updateStockTotal($model_name,$model_number,$stock_total){
    include('connect.php');
    if(!empty($model_number)){
      $sql = "UPDATE t_models SET stock_total = '$stock_total'
              WHERE model_name  = '$model_name' and model_number  = '$model_number'";
      if(mysqli_query($conn,$sql)){
        $sql2 = "UPDATE t_config SET update_date = now()";
        mysqli_query($conn,$sql2);
      }
    }

}

function getModelLocaltion($modelsId){
    include('connect.php');
    $localtionTxt = "";
    $sqlF = "SELECT * FROM t_fifo where  model_id = $modelsId
            and fifo_type = 'FI' ORDER BY fi_date";
    $queryF = mysqli_query($conn,$sqlF);
    $numF = mysqli_num_rows($queryF);
    for ($i=1; $i <= $numF ; $i++) {
      $rowF = mysqli_fetch_assoc($queryF);
      if($localtionTxt == ""){
        $localtionTxt = $rowF['location'];
      }else{
        $localtionTxt .= ", ".$rowF['location'];
      }
    }
    return $localtionTxt;
}

function getModelLocaltionV3($modelsId, $limit){
    include('connect.php');
    $localtionTxt = "";
    $sqlF = "SELECT * FROM t_fifo where  model_id = $modelsId
            and fifo_type = 'FI' ORDER BY fi_date";
    $queryF = mysqli_query($conn,$sqlF);
    $numF = mysqli_num_rows($queryF);
    for ($i=1; $i <= $numF ; $i++) {
      $rowF = mysqli_fetch_assoc($queryF);
      if($localtionTxt == ""){
        $localtionTxt = $rowF['location'];
      }else{
        if($i==$limit){
          $localtionTxt .= ",<br>".$rowF['location'];
        }else{
          $localtionTxt .= ", ".$rowF['location'];
        }

      }
    }
    return $localtionTxt;
}


function getModelLocaltionV2($modelsId, $limit){
    include('connect.php');
    $localtionTxt = "";
    $num  = 0;
    $sqlF = "SELECT * FROM t_fifo where  model_id = $modelsId
            and fifo_type = 'FI' ORDER BY fi_date";
    $queryF = mysqli_query($conn,$sqlF);
    $numF = mysqli_num_rows($queryF);
    for ($i=1; $i <= $numF ; $i++) {
      $rowF = mysqli_fetch_assoc($queryF);
      if($localtionTxt == ""){
        $localtionTxt = $rowF['location'];
      }else{
        $localtionTxt .= ", ".$rowF['location'];
      }
      if($limit == $i){
        $limit = ($limit + $i);
        $localtionArr[$num] = $localtionTxt;
        $localtionTxt = "";
        $num++;
      }
    }
    if($localtionTxt != ""){
      $localtionArr[$num] = $localtionTxt;
    }

    return $localtionArr;
}


function getFOLocaltion($modelsId){
    include('connect.php');
    $str = "";
    $strTmp ="";
    $sqlF = "SELECT * FROM t_fifo where  model_id = $modelsId
            and fifo_type = 'FI'  ORDER BY fi_date";
    $queryF = mysqli_query($conn,$sqlF);
    $numF = mysqli_num_rows($queryF);


    for ($i=1; $i <= $numF ; $i++) {
      $rowF = mysqli_fetch_assoc($queryF);
      $location     = $rowF['location'];
      $fifo_id      = $rowF['fifo_id'];
      $btnId        = "btn_".$fifo_id;
      $str .= "<div class='col-md-4'><p>";
      $str .= "  <button type='button' id='$btnId' onclick=\"setLocat('$btnId',$fifo_id,'$location')\"";
      $str .= "   class='btn btn-default btn-block btn-flat'>";
      $str .= "    <div class='text-cut' data-toggle='tooltip' title='$location'>$location</div>";
      $str .= "  </button></p></div>";
    }
    if($str != ""){
      $strTmp .= "<div class='col-md-12'><div class='form-group'>";
      $strTmp .=  "<label>Location</label><div class='row' style='padding-top:10px;'>";
      $strTmp .= $str;
      $strTmp .= "</div></div></div>";
      $str     = $strTmp;
    }
    return $str;
}



function convert($number){
  $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
  $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
  $number = str_replace(",","",$number);
  $number = str_replace(" ","",$number);
  $number = str_replace("บาท","",$number);
  $number = explode(".",$number);
  if(sizeof($number)>2){
    $number = number_format($number, 2);
  }

  $strlen = strlen($number[0]);
  $convert = '';
  for($i=0;$i<$strlen;$i++){
  	$n = substr($number[0], $i,1);
  	if($n!=0){
  		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
  		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
  		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
  		else{ $convert .= $txtnum1[$n]; }
  		$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'บาท';
  if($number[1]=='0' || $number[1]=='00' || $number[1]==''){
  $convert .= 'ถ้วน';
  }else{
  $strlen = strlen($number[1]);
  for($i=0;$i<$strlen;$i++){
  $n = substr($number[1], $i,1);
  	if($n!=0){
  	if($i==($strlen-1) AND $n==1){$convert
  	.= 'เอ็ด';}
  	elseif($i==($strlen-2) AND
  	$n==2){$convert .= 'ยี่';}
  	elseif($i==($strlen-2) AND
  	$n==1){$convert .= '';}
  	else{ $convert .= $txtnum1[$n];}
  	$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'สตางค์';
  }
  return $convert;
}


function resizeImageToBase64($obj,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {
    if(getimagesize($obj['tmp_name']))
    {
      $ext = pathinfo($obj["name"], PATHINFO_EXTENSION);
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = "thumbnails_".$user_id_update;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }


          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);

          $image   = file_get_contents($path.$new_images);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }else{
          $image   = file_get_contents($path.$name);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }

      }
    }
  }
  return $img;
}

function getTypeActive($typeActive){
  $unitName = "";
  if($typeActive == 1){
      $unitName = "วันที่ชำระเงิน";
  }else if($typeActive == 2){
      $unitName = "วันที่ Check-In ครั้งแรก";
  }else if($typeActive == 3){
      $unitName = "กำหนดเอง";
  }else{
      $unitName = "";
  }
  return $unitName;
}

function getUnitName($package_unit){
  $unitName = "";
  if($package_unit == 1){
      $unitName = "วัน";
  }else if($package_unit == 2){
      $unitName = "เดือน";
  }else if($package_unit == 3){
      $unitName = "ปี";
  }else if($package_unit == 4){
      $unitName = "ครั้ง";
  }else if($package_unit == 5){
      $unitName = "ตลอดชีพ";
  }else{
      $unitName = "";
  }
  return $unitName;
}

function resizeImageBase64($imgBase,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($imgBase) && !empty($imgBase))
  {
    if(!empty($h) || !empty($w))
    {
      $imageOrig      = imagecreatefromstring(base64_decode($imgBase));
      $new_images     = "thumbnails_".$user_id_update;

      $x  = 0;
      $y  = 0;

      $width_orig   = 400;
      $height_orig  = 300;

      if(empty($h)){
          if($width_orig > $w){
            $new_height  = $height_orig*($w/$width_orig);
            $new_width   = $w;
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else if(empty($w))
      {
          if($height_orig > $h){
            $new_height  = $h;
            $new_width   = $width_orig*($h/$height_orig);
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else
      {
        if($height_orig > $width_orig)
        {
          $new_height  = $height_orig*($w/$width_orig);
          $new_width   = $w;
        }else{
          $new_height  = $h;
          $new_width   = $width_orig*($h/$height_orig);
        }
      }

      $create_width   =  $new_width;
      $create_height  =  $new_height;


      if(!empty($h) && $new_height > $h){
         $create_height = $h;
      }

      if(!empty($w) && $new_width > $w){
        $create_width = $w;
      }


      $imageOrig;
      $imageResize    = imagecreatetruecolor($create_width, $create_height);
      $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
      imagecolortransparent($imageResize, $background);
      imagealphablending($imageResize, false);
      imagesavealpha($imageResize, true);

      $new_images     = $new_images.".png";
      imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
      imagepng($imageResize,$path.$new_images);


      imagedestroy($imageOrig);
      imagedestroy($imageResize);

      $image   = file_get_contents($path.$new_images);
      $img     = 'data:image/png;base64,'.base64_encode($image);
    }
  }
  return $img;
}

function formatDateTh($date){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date("d/m/Y", $d);
  }
  return $Datestr;
}

function setFormatDate($date,$format){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date($format, $d);
  }
  return $Datestr;
}

function chkNull($obj){
  if(!isset($obj) || empty($obj)){
    $obj = "";
  }
  return $obj;
}

?>
