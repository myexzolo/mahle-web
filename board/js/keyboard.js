var ID = "";
var LANGUAGE = "EN";
var POSITION = 0;
var SHIFT = "";

function setLanguage(){
  SHIFT = "";
  if(LANGUAGE == "EN"){
    LANGUAGE = "TH";
    keypad('th');
  }else{
    LANGUAGE = "EN";
    keypad('en');
  }
}

function setId(idInput){
  //alert(idInput);
  ID = idInput;
  var id = '#'+ ID;
  var obj = $(id)[0];
  POSITION = doGetCaretPosition(obj);
  if(idInput == "phone"){
    keypad('numpad');
  }else{
    keypad('en');
  }
  //alert(POSITION);
}

function keypad(type){
  //alert(type);
  if(type == 'number'){
    LANGUAGE = "NUM";
    // $.post("keypad_num.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }else if(type == 'numpad'){
    // $.post("numberpad.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }else if(type == 'enShift'){
    SHIFT = "Y";
    LANGUAGE = "EN";
    // $.post("keypad_en_shift.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }else if(type == 'en'){
    LANGUAGE = "EN";
    // $.post("keypad_en.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }else if(type == 'th'){
    LANGUAGE = "TH";
    // $.post("keypad_th.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }else if(type == 'thShift'){
    LANGUAGE = "TH";
    SHIFT = "Y";
    // $.post("keypad_th_shift.php")
    //   .done(function( data ) {
    //     $('#keyboard').html(data);
    // });
  }
}

function keyPos(type){
  var idinput = '#'+ ID;
  if(type == 'back'){
    POSITION--;
    $(idinput).focus();
    $(idinput).setCursorPosition(POSITION);
  }else if(type == 'next'){
    var numval  = $(idinput).val().length;
    $(idinput).focus();
    if(POSITION < numval){
      POSITION++;
      $(idinput).setCursorPosition(POSITION);
    }

  }
}

function deltext(){
  if(ID != ""){
    var idinput = '#'+ ID;
    //var val = $(idinput).val() + str;
    //alert(val);
    POSITION = doGetCaretPosition($(idinput)[0]);
    var val       = $(idinput).val();
    var startText     = val.substring(0, (POSITION - 1));
    var numstartText  = startText.length;
    var numval        = val.length;
    var numEndText    = (numval - numstartText);
    //alert(numstartText+"<  numval:"+numval+" ,numEndText:"+numEndText);

    var endText = "";
    if(numEndText > 0){
      endText  = val.substring(POSITION);
    }

    //alert(startText+"<  >"+endText);
    $(idinput).val(startText+endText);
    if(SHIFT == "Y" && LANGUAGE == "EN"){
      keypad('en');
    }else if(SHIFT == "Y" && LANGUAGE == "TH"){
      keypad('th');
    }else if(LANGUAGE == "NUM"){
      keypad('number');
    }
    $(idinput).focus();
    POSITION--;
    $(idinput).setCursorPosition(POSITION);
  }
}

function addtext(str){
  if(ID != ""){
    var idinput = '#'+ ID;
    //var val = $(idinput).val() + str;
    //alert(val);
    POSITION = doGetCaretPosition($(idinput)[0]);
    var val       = $(idinput).val();
    var startText     = val.substring(0, POSITION);
    var numstartText  = startText.length;
    var numval        = val.length;
    var numEndText    = (numval - numstartText);
    //alert(numstartText+"<  numval:"+numval+" ,numEndText:"+numEndText);

    var endText = "";
    if(numEndText > 0){
      endText  = val.substring(POSITION);
    }

    //alert(startText+"<  >"+endText);
    $(idinput).val(startText+str+endText);
    if(SHIFT == "Y" && LANGUAGE == "EN"){
      keypad('en');
    }else if(SHIFT == "Y" && LANGUAGE == "TH"){
      keypad('th');
    }else if(LANGUAGE == "NUM"){
      keypad('number');
    }
    $(idinput).focus();
    POSITION++;
    $(idinput).setCursorPosition(POSITION);
  }
}

$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};

function doGetCaretPosition (oField) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    oField.focus();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange();

    // Move selection start to 0 position
    oSel.moveStart('character', -oField.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (oField.selectionStart || oField.selectionStart == '0')
    iCaretPos = oField.selectionStart;

  // Return results
  return iCaretPos;
}
