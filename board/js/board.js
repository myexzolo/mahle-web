var page = 0;
TIMEDISPLAY = 0;
function showBoard(numPage,seq){
  $.post("ajax/showBoard.php",{numPage:numPage,seq:seq})
    .done(function( data ) {
      $('#show-page').html(data);
      numPage = $('#numList').val();
      seq     = $('#seq').val();
      page += numPage;
      if(numPage >= 15){
        fadeIn();
        setTimeout(function(){
          fadeOut();
          getConfigBoard();
          setTimeout(function(){
            showBoard(page,seq);
          }, 5000);
        }, TIMEDISPLAY);
      }else if(numPage > 0){
        fadeIn();
        setTimeout(function(){
          fadeOut();
          getConfigBoard();
          setTimeout(function(){
            showBoard(page,seq);
          }, 5000);
        }, TIMEDISPLAY)
      }else if(numPage == 0){
        page = 0;
        seq = 0;
        getConfigBoard();
        showBoard(page,seq);
      }
  });
}

function getConfigBoard(){
  $.get( "ajax/getConfigBoard.php", function( data ) {
    TIMEDISPLAY = (data.time_show * 1000);
    $('#responsibility').html(data.responsibility);
    var dateTime = data.update_date;
    var d = dateTime.split(" ");
    var dateTh = d[0].split("-");
    $('#timeUpdate').html(d[1]);
    $('#dateUpdate').html(dateTh[2] + "/" + dateTh[1] + "/" + dateTh[0]);
  });
}


function fadeIn(){
  $('.fadeIn').css('visibility', 'visible').animate({opacity: 1.0}, 3000);
}

function fadeOut(){
  $('.fadeIn').animate({opacity: 0}, 3000);
}
