function gopageFi(){
  var url = "groupModels.php?type=FI";
  window.location = url;
}

function gopageFo(){
  var url = "groupModels.php?type=FO";
  window.location = url;
}

function gopage(url){
  //alert(url);
  window.location = url;
}
function goPageFiFo(modelId,groupModelId){
  var type = $('#type').val();
  var pam  = "?type="+type+"&&modelId="+modelId+"&&groupModelId="+groupModelId;
  var url  = "";
  if(type == "FO"){
    url  = "modelsFo.php" + pam;
  }else{
    url  = "fifo.php" + pam;
  }

  window.location = url;
}

function saveLocation()
{
  if($('#matLocation').val() != ""){
    var content = "";
    content += "<div class='col-md-4' align='center'>" + $('#modelDetail1').html() + "</div>";
    content += "<div class='col-md-8' >" + $('#modelDetail2').html();
    content += "<p>Localtion : "+ $('#matLocation').val() +"</p>";
    content += "</div>";
    $.confirm({
        title: 'ยืนยันการบันทึก First IN',
        content: content,
        icon: 'fa fa-warning',
        columnClass: 'col-md-6',
        theme: 'material',
        closeIcon: false,
        draggable: true,
        animation: 'zoom',
        closeAnimation: 'opacity',
        type: 'dark',
        buttons: {
            somethingElse: {
                text: '  ยืนยัน  ',
                btnClass: 'btn-blue buttonsConfirm',
                action: function(){
                  $('#formAdd').submit();
                }
            },
            cancel:{
              text: '  ยกเลิก  ',
              btnClass: 'btn-default buttonsConfirm'  //$.alert('Canceled!');
            },
        }
    });
  }
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: '../admin/ajax/fifo/manageFiFo.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        //alert(data.message);
        gopage("index.php");
    });
  }else{
    $('#matLocation').focus();
  }
});

function urldecode(str) {
   return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}

function setFo(fifo_id,model_id,location,type,content){
  var decContent = urldecode(content);
  $.confirm({
      title: 'ยืนยันการบันทึก First OUT',
      content: decContent,
      columnClass: 'col-md-6',
      icon: 'fa fa-warning',
      theme: 'material',
      closeIcon: false,
      draggable: true,
      animation: 'zoom',
      closeAnimation: 'opacity',
      type: 'dark',
      buttons: {
          somethingElse: {
              text: '  ยืนยัน  ',
              btnClass: 'btn-blue buttonsConfirm',
              action: function(){
                $.post("../admin/ajax/fifo/manageFiFo.php",{fifo_id:fifo_id,model_id:model_id,location:location,fifo_type:type})
                  .done(function( data ) {
                    gopage("index.php");
                });
              }
          },
          cancel:{
            text: '  ยกเลิก  ',
            btnClass: 'btn-default buttonsConfirm'  //$.alert('Canceled!');
          },
      }
  });
}

function reset(){
  $('#matLocation').val("");
  $('#matLocation').focus();
}

function goPageModel(groupModelId){
  var type = $('#type').val();
  var pam  = "?type="+type+"&&groupModelId="+groupModelId;
  var url  = "models.php" + pam;
  window.location = url;
}


var PAGENUM   = 0;
var PAGEMAX   = 0;
var PAGETOTAL   = 0;
var NUMLIMIT    = 10;
var NUMLIMIT2   = 9;



function showGroupModel(page){
  if(page == "minus" && PAGENUM > 0){
    PAGENUM--;
  }else if (page == "plus" && PAGEMAX == NUMLIMIT){
    PAGENUM++;
  }else if (page == "plus" && PAGEMAX < NUMLIMIT){
    PAGEMAX = 0;
  }else if (page == "minus" && PAGENUM <= 0){
    PAGENUM = 0;
  }else{
    PAGENUM = page;
  }
  $.post("ajax/showGroupModels.php",{page:PAGENUM,numLimit:NUMLIMIT})
    .done(function( data ) {
      $('#show-groupModel').html(data);
      PAGEMAX   = $('#numData').val();
      PAGETOTAL = $('#numTotalData').val();

      if(PAGETOTAL > NUMLIMIT && PAGEMAX >=  NUMLIMIT){
        $('#btnPlus').prop("disabled", false);
        $('#btnMinus').prop("disabled", true);
      }else{
        $('#btnPlus').prop("disabled", true);
        $('#btnMinus').prop("disabled", false);
      }
  });
}

function showModel(page){
  if(page == "minus" && PAGENUM > 0){
    PAGENUM--;
  }else if (page == "plus" && PAGEMAX == NUMLIMIT2){
    PAGENUM++;
  }else if (page == "plus" && PAGEMAX < NUMLIMIT2){
    PAGEMAX = 0;
  }else if (page == "minus" && PAGENUM <= 0){
    PAGENUM = 0;
  }else{
    PAGENUM = page;
  }

  $.post("ajax/showModels.php",{page:PAGENUM,numLimit:NUMLIMIT2,groupModelId:$('#groupModelId').val()})
    .done(function( data ) {
      $('#show-Model').html(data);
      PAGEMAX   = $('#numData').val();
      PAGETOTAL = $('#numTotalData').val();

      if(PAGETOTAL > NUMLIMIT2 && PAGEMAX >=  NUMLIMIT2){
        $('#btnPlus').prop("disabled", false);
        $('#btnMinus').prop("disabled", true);
      }else{
        $('#btnPlus').prop("disabled", true);
        if(parseInt(PAGETOTAL) > parseInt(PAGEMAX)){
          $('#btnMinus').prop("disabled", false);
        }
      }
  });
}

function showModelFo(page){

  if(page == "minus" && PAGENUM > 0){
    PAGENUM--;
  }else if (page == "plus" && PAGEMAX == NUMLIMIT2){
    PAGENUM++;
  }else if (page == "plus" && PAGEMAX < NUMLIMIT2){
    PAGEMAX = 0;
  }else if (page == "minus" && PAGENUM <= 0){
    PAGENUM = 0;
  }else{
    PAGENUM = page;
  }


  $.post("ajax/showLocation.php",{page:PAGENUM,numLimit:NUMLIMIT2,modelId:$('#modelId').val()})
    .done(function( data ) {
      $('#show-Model').html(data);
      PAGEMAX   = $('#numData').val();
      PAGETOTAL = $('#numTotalData').val();
      //alert(PAGEMAX+","+PAGETOTAL+","+NUMLIMIT2);

      if(PAGETOTAL > NUMLIMIT2 && PAGEMAX >=  NUMLIMIT2){
        $('#btnPlus').prop("disabled", false);
        $('#btnMinus').prop("disabled", true);
      }else{
        $('#btnPlus').prop("disabled", true);
        if(parseInt(PAGETOTAL) > parseInt(PAGEMAX)){
          $('#btnMinus').prop("disabled", false);
        }
      }
  });

}
