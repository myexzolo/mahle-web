<!DOCTYPE html>
<html>
<?php
  include("inc/head.php");

  $type         = isset($_GET['type'])?$_GET['type']:"";
  $modelId      = isset($_GET['modelId'])?$_GET['modelId']:"";
  $groupModelId = isset($_GET['groupModelId'])?$_GET['groupModelId']:"";
  if($type == 'FO'){
    $typeName = "FIRST OUT";
  }else {
    $typeName = "FIRST IN";
  }

  $sql = "SELECT * FROM  t_models m, t_group_models gm
  where m.is_active = 'Y' and gm.is_active = 'Y' and m.model_id = $modelId
  and gm.group_model_id = m.group_model_id";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $row = mysqli_fetch_assoc($query);
  $group_model_img  = $row['group_model_img'];
  $group_model_id   = $row['group_model_id'];
  $model_id         = $row['model_id'];
  $model_name       = $row['model_name'];
  $model_number     = $row['model_number'];
?>
<body class="animated bounceInRight" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;">
  <div class="section logo">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <img src="src/images/logo.png" class="img-fluid my-4"> </div>
      </div>
    </div>
  </div>
  <form id="formAdd" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
  <div class="section update">
    <div class="container-fluid">
      <div class="row" id="divShow">
        <div class="col-md-12">
          <table style="width:100%">
            <tr>
              <td style="width:9vw">&nbsp;</td>
              <td style="width:15vw;">
                <div align="left" class="card-icon">
                    <div class="col-md-4" align="center" id="modelDetail1">
                      <img src="<?=$group_model_img ?>"> </div>
                    <div class="col-md-8" id="modelDetail2" style="margin-top:1.5vh">
                      <p>Name : <?=$model_name ?></p>
                      <p>Number : <?=$model_number ?></p>
                    </div>
                </div>
              </td>
              <td align="center">
                <div align="center" class="input-bg">
                    <input style="font-size: 3.5vw;" id="matLocation" name="location" type="text" maxlength="4" required>
                </div>
              </td>
              <td style="width:15vw">
                <div align="right">
                    <button class="btn-onclick" type="button" onclick="saveLocation()">OK</button>
                    <div style="height:2vh;"></div>
                    <button class="btn-onclick" onclick="reset()">CENCEL</button>
                </div>
              </td>
              <td style="width:9vw">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="col-md-12">
          <table style="width:100%">
            <tr>
              <td style="width:9vw">&nbsp;</td>
              <td align="left">
                <div class="rowkeypad" style="width: 100%; height:30vh; margin-top: 5vh;">
                  <div style="width: 100%;" align="center">
                    <button type="button" class="keytext" onclick="addtext('Q')">Q</button>
                    <button type="button" class="keytext" onclick="addtext('W')">W</button>
                    <button type="button" class="keytext" onclick="addtext('E')">E</button>
                    <button type="button" class="keytext" onclick="addtext('R')">R</button>
                    <button type="button" class="keytext" onclick="addtext('T')">T</button>
                    <button type="button" class="keytext" onclick="addtext('Y')">Y</button>
                    <button type="button" class="keytext" onclick="addtext('U')">U</button>
                    <button type="button" class="keytext" onclick="addtext('I')">I</button>
                    <button type="button" class="keytext" onclick="addtext('O')">O</button>
                    <button type="button" class="keytext" onclick="addtext('P')">P</button>
                    <button type="button" class="keybackspace" onclick="deltext()">Backspace</button>
                  </div>
                  <div class="container2" style="width: 100%; padding: 4px;" align="center">
                    <button type="button" class="keytext" onclick="addtext('A')">A</button>
                    <button type="button" class="keytext" onclick="addtext('S')">S</button>
                    <button type="button" class="keytext" onclick="addtext('D')">D</button>
                    <button type="button" class="keytext" onclick="addtext('F')">F</button>
                    <button type="button" class="keytext" onclick="addtext('G')">G</button>
                    <button type="button" class="keytext" onclick="addtext('H')">H</button>
                    <button type="button" class="keytext" onclick="addtext('J')">J</button>
                    <button type="button" class="keytext" onclick="addtext('K')">K</button>
                    <button type="button" class="keytext" onclick="addtext('L')">L</button>
                    <button type="button" class="keytext" onclick="addtext('_')">_</button>
                  </div>
                  <div class="container3" style="width: 100%; padding: 4px;" align="center">
                    <button type="button" class="keytext" onclick="addtext('Z')">Z</button>
                    <button type="button" class="keytext" onclick="addtext('X')">X</button>
                    <button type="button" class="keytext" onclick="addtext('C')">C</button>
                    <button type="button" class="keytext" onclick="addtext('V')">V</button>
                    <button type="button" class="keytext" onclick="addtext('B')">B</button>
                    <button type="button" class="keytext" onclick="addtext('N')">N</button>
                    <button type="button" class="keytext" onclick="addtext('M')">M</button>
                    <button type="button" class="keytext" onclick="addtext('.')">.</button>
                    <button type="button" class="keytext" onclick="addtext('-')">-</button>
                  </div>
                  <div class="container4" style="width: 100%; padding: 4px;" align="center">
                    <button type="button" class="keyspacebar">Spacebar</button>
                  </div>
                </div>
              </td>
              <td style="width:30vw">
                <div style="width: 17vw;margin-top: 5vh;height:30vh;" align="center">
                  <button type="button" class="numpad" onclick="addtext('1')">1</button>
                  <button type="button" class="numpad" onclick="addtext('2')">2</button>
                  <button type="button" class="numpad" onclick="addtext('3')">3</button>
                  <button type="button" class="numpad" onclick="addtext('4')">4</button>
                  <button type="button" class="numpad" onclick="addtext('5')">5</button>
                  <button type="button" class="numpad" onclick="addtext('6')">6</button>
                  <button type="button" class="numpad" onclick="addtext('7')">7</button>
                  <button type="button" class="numpad" onclick="addtext('8')">8</button>
                  <button type="button" class="numpad" onclick="addtext('9')">9</button>
                  <button type="button" class="numpad" onclick="addtext('0')">0</button>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3" style="padding-left:50px;"><a onclick="gopage('models.php?type=<?=$type?>&&groupModelId=<?=$groupModelId?>')"><i class="fa fa-arrow-left txtBack"></i></a></div>
    <div class="col-md-6" align="center"><div class="txtMenu"><?= $typeName ?></div></div>
    <div class="col-md-3" style="padding-right:50px;"><a onclick="gopage('index.php')"><i class="fa fa-home pull-right txtHome"></i></a></div>
</div>
<input type="hidden" value="<?= $type?>" id="type" name="fifo_type">
<input type="hidden" value="<?= $groupModelId?>" id="groupModelId">
<input type="hidden" value="<?= $modelId?>" id="modelId" name="model_id">
</form>
</body>
<?php include("inc/footer.php"); ?>
<script src="js/models.js"></script>
<script src="js/keyboard.js"></script>
<script>
  $( document ).ready(function() {
    $(function() {
      $('#divShow').css('height',"60vh");
      setId('matLocation');
      setTimeout(function(){
        $('#matLocation').focus();
      }, 1000);
    });
  });
</script>
</html>
