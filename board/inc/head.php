<?php
  session_start();
  include('../conf/connect.php');
  include('../conf/utils.php');
  ?>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>FIFO MANAGEMENT</title>

      <link rel="shortcut icon" type="image/png" href="../images/logo_ico.png"/>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="../bootstrap/css/main.css">
      <!-- Smoke -->
      <link rel="stylesheet" href="../css/smoke.css">

      <link rel="stylesheet" href="../css/animate.css" type="text/css">

      <!-- lightbox -->
      <link rel="stylesheet" href="../css/lightbox.css" >

      <!-- Font Awesome -->
      <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="../Ionicons/css/ionicons.min.css">

      <!-- fullCalendar -->
      <link rel="stylesheet" href="../fullcalendar/dist/fullcalendar.min.css">
      <link rel="stylesheet" href="../fullcalendar/dist/fullcalendar.print.min.css" media="print">

      <!-- daterange picker -->
      <link rel="stylesheet" href="../bootstrap-daterangepicker/daterangepicker.css">
      <!-- bootstrap datepicker -->
      <link rel="stylesheet" href="../bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
      <link rel="stylesheet" href="../css/jquery-confirm.min.css">
      <!-- iCheck for checkboxes and radio inputs -->
      <link rel="stylesheet" href="../iCheck/all.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">

  	  <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
      <link rel="stylesheet" href="../css/w3.css" type="text/css">

      <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">
      <link rel="stylesheet" href="src/css/theme.css" type="text/css">
      <link rel="stylesheet" href="src/css/keypad.css" type="text/css">

      <script src="js/jquery.min.js" ></script>
      <script src="js/jquery-idleTimeout-plus.js" type="text/javascript"></script>
</head>
