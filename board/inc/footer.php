<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dropup.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery-confirm.min.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/smoke.js"></script>

<script>
  new WOW().init();
  $(function() {
    IdleTimeoutPlus.start({
        idleTimeLimit: 100,
        warnEnabled: false,
        redirectCallback:true,
        redirectUrl: 'index.php'
    });
  });
</script>
