<!DOCTYPE html>
<html>
<?php
  include("inc/head.php");
?>
<body class="ibody">
  <div class="section head">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12" style="height:78px;line-height:78px;font-size:50px;height-line">FIFO MANAGEMENT SYSTEM-FINISHED GOODS AREA</div>
        <div class="col-md-12">
          <div class="container-fluid  mx-3">
            <div class="row">
              <div class="col-lg-4">
                <p>DATE :
                  <span class="txt-yellow" id="dateUpdate">DD/MM/YYYY</span>
                </p>
              </div>
              <div class="col-lg-4">
                <p>UPDATE TIME :
                  <span class="txt-yellow" id="timeUpdate">HH/MM/SS</span>
                </p>
              </div>
              <div class="col-lg-4">
                <p>RESPONSIBILITY :
                  <span class="txt-blue" id="responsibility">(20 MAX TYPE)</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div id= "show-page" style="width:100%"></div>
      </div>
    </div>
  </div>
</body>
<script src="js/board.js" type="text/javascript"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/dropup.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery-confirm.min.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/smoke.js"></script>
<script>
  getConfigBoard();
  showBoard(0,0);
</script>
</html>
