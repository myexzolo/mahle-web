<!DOCTYPE html>
<html>
<?php include("inc/head.php"); ?>
<body class="animated bounceInRight" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;">
  <div class="section logo">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <img src="src/images/logo.png" class="img-fluid my-4"> </div>
      </div>
    </div>
  </div>
  <div class="section mainbody">
    <div class="container-fluid box-btn">
      <div class="row">
        <div class="col-md-6">
          <a class="btn-p" style="padding-top:14vh;" onclick="gopageFi()">
            <img src="src/images/in.png" style="height:14vh;">
            <p>First In</p>
          </a>
        </div>
        <div class="col-md-6">
          <a class="btn-p" style="padding-top:14vh;" onclick="gopageFo()">
            <img src="src/images/out.png" style="height:14vh;">
            <p>First Out</p>
          </a>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="js/wow.min.js"></script>
<script src="js/models.js"></script>

</html>
