<!DOCTYPE html>
<html>

<?php
  include("inc/head.php");

  $type         = isset($_GET['type'])?$_GET['type']:"";
  $modelId      = isset($_GET['modelId'])?$_GET['modelId']:"";
  $groupModelId = isset($_GET['groupModelId'])?$_GET['groupModelId']:"";
  if($type == 'FO'){
    $typeName = "FIRST OUT";
  }else {
    $typeName = "FIRST IN";
  }
?>
<body class="animated bounceInRight" >
  <div class="section logo">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <img src="src/images/logo.png" class="img-fluid my-4"> </div>
      </div>
    </div>
  </div>
  <div class="section mainbody">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-1 disabled">
          <button id="btnMinus" class="btn2-nb" disabled  onclick="showModelFo('minus')"><img src="src/images/back_ico.png"> </button>
        </div>
        <??>
        <div class="col-md-10" id="divShow" style="width:100%;">
          <div id="show-Model" align="center"></div>
        </div>
        <div class="col-md-1">
          <button id="btnPlus" class="btn2-nb" disabled onclick="showModelFo('plus')"><img src="src/images/next_ico.png"> </button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3" style="padding-left:50px;"><a onclick="gopage('models.php?type=<?=$type?>&&groupModelId=<?=$groupModelId?>')"><i class="fa fa-arrow-left txtBack"></i></a></div>
    <div class="col-md-6" align="center"><div class="txtMenu"><?= $typeName ?></div></div>
    <div class="col-md-3" style="padding-right:50px;"><a onclick="gopage('index.php')"><i class="fa fa-home pull-right txtHome"></i></a></div>
</div>
<input type="hidden" value="<?= $type?>" id="type" name="fifo_type">
<input type="hidden" value="<?= $groupModelId?>" id="groupModelId">
<input type="hidden" value="<?= $modelId?>" id="modelId" name="model_id">
</body>
<?php include("inc/footer.php"); ?>
<script src="js/models.js"></script>
<script>
  $( document ).ready(function() {
    showModelFo(0);
    $('#divShow').css('height',"60vh");
  });
</script>
</html>
