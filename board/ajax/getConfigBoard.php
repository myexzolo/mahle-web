<?php
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$sql = "SELECT * FROM t_config";
$query = mysqli_query($conn,$sql);

if($query){
  $row   = mysqli_fetch_assoc($query);
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
