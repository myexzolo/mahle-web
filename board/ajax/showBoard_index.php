<?php
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$numPage = isset($_POST['numPage'])?$_POST['numPage']:"";
$con  = "";
if($numPage != ""){
  $con = "LIMIT $numPage,15";
}else{
  $con = "LIMIT 0,15";
}


$sql = "SELECT * FROM t_models m, t_group_models gm
        where m.group_model_id = gm.group_model_id
        and m.is_active = 'Y' and gm.is_active = 'Y'
        ORDER BY gm.seq*1,m.seq*1 ". $con;
$query = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);
$numList = $num;
$classTR = "";
?>
<table style="width:100%;" align="center">
  <tr class="tr1" style="border-bottom: 3px #030303 solid;">
    <td class="thboard" style="width:5%;font-size:16px" rowspan="2">S.NO</td>
    <td class="thboard" style="width:32%;font-size:16px" rowspan="2">MODEL</td>
    <td class="thboard" style="width:12%;font-size:16px" colspan="2">STD STOCK</td>
    <td class="thboard" style="width:6%;font-size:16px" rowspan="2">TOTAL<br>STOCK</td>
    <td class="thboard" style="font-size:16px" colspan="2">STATUS</td>
    <td class="thboard2" style="padding: 5px;font-size:16px" rowspan="2">MATERIAL STORAGE LOCATION<br>MATERIAL MOVEMENT</td>
  </tr>
  <tr class="tr1" style="border-bottom: 3px #030303 solid;font-size:16px">
    <td class="thboard" style="width:6%;font-size:16px">MAX</td>
    <td class="thboard" style="width:6%;font-size:16px">MIN</td>
    <td class="thboard" style="width:7%;font-size:16px">QUANTITY</td>
    <td class="thboard" style="font-size:16px">COLOR</td>
  </tr>
  <tbody>
<?php
for ($i=1; $i <= $num ; $i++) {
  $row = mysqli_fetch_assoc($query);

  $group_model_img  = isset($row['group_model_img'])?$row['group_model_img']:"";
  $model_id         = isset($row['model_id'])?$row['model_id']:"";
  $model_name       = isset($row['model_name'])?$row['model_name']:"";
  $model_number     = isset($row['model_name'])?$row['model_number']:"";
  $stock_min        = isset($row['stock_min'])?$row['stock_min']:"";
  $stock_max        = isset($row['stock_min'])?$row['stock_max']:"";
  $stock_total      = isset($row['stock_min'])?$row['stock_total']:"";

  if($classTR == "tr0"){
    $classTR = "tr1";
  }else{
    $classTR = "tr0";
  }
  $logo = "";
  if($group_model_img != ""){
    $logo = "<img src='$group_model_img' style='width:auto;height:3.5vh;'>";
  }
  $listFi = "";
  $quantity = "";
  $textColor = "txt-green";
  if($model_id != ""){

    if($stock_total > $stock_max){
      $textColor = "txt-yellow";
      $quantity  = number_format($stock_total - $stock_max);
    }else if($stock_total < $stock_min){
      $textColor = "txt-red";
      $quantity  = number_format($stock_total - $stock_min);
    }

    $stock_min = number_format($stock_min);
    $stock_max = number_format($stock_max);
    $stock_total = number_format($stock_total);
    //echo $model_id;
    $listFi = getModelLocaltion($model_id);
  }
?>
<tr class='<?= $classTR ?>' >
  <td class="tbboard" style="font-size:16px;"><div class="fadeIn"><?= ($i+$numPage) ?></div></td>
  <td class="tbboard">
    <div class="p-0 datalist fadeIn">
      <div class="col-md-3" style="float: left;"><?= $logo ?></div>
      <div class="col-md-4" style="float: left; text-align: left; padding-left: 20px;font-size:16px;"><?= $model_name ?></div>
      <div class="col-md-5" style="float: left; text-align: left;font-size:16px;"><?= $model_number ?></div>
    </div>
  </td>
  <td style="text-align:right;padding-right:10px;font-size:16px;" class="tbboard"><div class="fadeIn"><?= $stock_max ?></div></td>
  <td style="text-align:right;padding-right:10px;font-size:16px;" class="tbboard"><div class="fadeIn"><?= $stock_min ?></div></td>
  <td style="text-align:right;padding-right:10px;font-size:16px;" class="tbboard  txt-blue"><div class="fadeIn"><?= $stock_total ?></div></td>
  <td style="text-align:right;padding-right:10px;font-size:16px;" class="tbboard <?= $textColor ?>"><div class="fadeIn"><?= $quantity ?></div></td>
  <td class="tbboard"><div class="fadeIn circleStatus"><i class="fa fa-circle <?= $textColor ?> " ></div></td>
  <td class="tbboard2" style="padding-left:15px;font-size:16px;" align="left"><div class="fadeIn"><?=$listFi ?></div></td>
</tr>
<?php }
if($num < 15){
  $num++;
  for ($i=$num; $i <= 15 ; $i++) {
    if($classTR == "tr0"){
      $classTR = "tr1";
    }else{
      $classTR = "tr0";
    }
  ?>
    <tr class='<?= $classTR ?> '>
      <td class="tbboard" style="font-size:16px"><div class="fadeIn"><?= ($i+$numPage) ?></div></td>
      <td class="tbboard"></td>
      <td style="text-align:right;padding-right:10px;" class="tbboard"></td>
      <td style="text-align:right;padding-right:10px;" class="tbboard"></td>
      <td style="text-align:right;padding-right:10px;" class="tbboard"></td>
      <td style="text-align:right;padding-right:10px;" class="tbboard"></td>
      <td class="tbboard"></td>
      <td class="tbboard2"></td>
    </tr>
  <?php
    }
  }
 ?>
</tbody>
</table>
<input type="hidden" id="numList" value="<?=$numList?>">
