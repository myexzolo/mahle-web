
  function showSlide(){
    $.post("ajax/slide/showSlide.php")
      .done(function( data ) {
        sortableData();
        $('#sortable').html(data);
    });
  }
  function sortableData(){
    $( "#sortable" ).sortable({
      placeholder: "ui-state-highlight"
    });
    $( "#sortable" ).disableSelection();
  }

  function formSlide(value=""){
    $.post("ajax/slide/formSlide.php",{value:value})
      .done(function( data ) {
        $('#myModal').modal('toggle');
        $('#form-slide').html(data);
    });
  }
  function removeSlide(value){
    $.smkConfirm({
      text:'Are You Sure?',
      accept:'Yes',
      cancel:'Cancel'
    },function(res){
      // Code here
      if (res) {
        $.post("ajax/slide/delSlide.php",{value:value})
          .done(function( data ) {
            $.smkProgressBar();
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              showSlide();
              $.smkAlert({text: data.message,type: data.status});
            }, 1000);
        });
      }
    });
  }

  $(document).ready(function() {
    $("#panel2").smkPanel({
      hide: 'full,remove'
    });
    showSlide();
    $('#form-slide').on('submit', function(event) {
      event.preventDefault();
      if ($('#form-slide').smkValidate()) {

        if($( "input[name='ts_id']" ).val() == ""){
          if($( "input[name='image']" ).val() == ""){
            $.smkAlert({
              text: 'PLEASE INPUT YOUR FILE.',
              type: 'warning',
            });
          }
        }

          $.ajax({
              url: 'ajax/slide/manageSlide.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            $.smkProgressBar();
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              $('#form-slide').smkClear();
              $('.image').val();
              showSlide();
              $.smkAlert({text: data.message,type: data.status});
              $('#myModal').modal('toggle');
            }, 1000);
          });

      }
    });
    $('#lenght-slide').on('submit', function(event) {
      event.preventDefault();
      /* Act on the event */
      $.post("ajax/slide/lengthSlide.php", $('#lenght-slide').serialize())
        .done(function( data ) {
        $.smkAlert({text: data.message,type: data.status});
        showSlide();
      });
    });

  });
