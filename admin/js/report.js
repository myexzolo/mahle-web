function show(){
  $.post("ajax/report/report1.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

$(document).ready(function() {
  show();
});

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplay").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
