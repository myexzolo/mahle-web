function showForm(modelsId,type){
  var strType = "";
  if(type == "FI"){
    strType = "First In";
  }else{
    strType = "First Out";
  }
  var header = "Management " + strType;
  $('#myModalLabel').html(header);
  $.post("ajax/fifo/formFiFo.php",{modelsId:modelsId,type:type})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}

function showFormFi(fifoId,location){
  $.post("ajax/fifo/formFi.php",{fifoId:fifoId,location:location})
    .done(function( data ) {
      $('#myModal2').modal('toggle');
      $('#show-form2').html(data);
  });
}

function setLocat(btnId,fifoId,location){
  $(".bg-navy").removeClass("bg-navy").addClass("btn-default");
  var fifo_id = $('#fifo_id').val();
  //alert(fifo_id+", "+ fifoId);
  if(fifo_id != fifoId){
     $('#fifo_id').val(fifoId);
     $('#location').val(location);
     $('#'+btnId).removeClass('btn-default').addClass('bg-navy');
  }else{
    $('#fifo_id').val("");
    $('#location').val("");
    $('#'+btnId).removeClass('bg-navy').addClass('btn-default');
  }

}

function removeFi(fifoId){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      //alert(groupModelsId);
      $.post("ajax/fifo/delFi.php",{fifoId:fifoId})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showFormModel(MODELID,NAME);
            MODELID = "";
            NAME    = "";
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}



function showPageFo(){
  $.post("ajax/fifo/showfo.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function cencelFo(id,groupModelImg,modelName,modelNumber,location){
  var str = "<P>ต้องการยกเลิก FO ?</p>"+
            "<p>Model Name : "+modelName+"</p>"+
            "<p>Number : "+modelNumber+"</p>"+
            "<p>Location : "+location+"</p>";
  $.smkConfirm({
    text:str ,
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      //alert(groupModelsId);
      $.post("ajax/fifo/cencelFo.php",{fifoId:id})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPageFo();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


function showPage(){
  MODELID = "";
  NAME    = "";
  var header = "Management FIFO";
  $('#nameHeader').html(header);
  $.post("ajax/fifo/showfifo.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}
var MODELID = "";
var NAME = "";
function showFormModel(modelId,name){
  var header = "<button class='btn btn-success btn-flat btnBack' onclick='showPage()'><i style='font-size:12px' class='fa fa-mail-reply'></i> Back</button>"
    +"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;Management FIFO Name : " + name;
  $('#nameHeader').html(header);
  MODELID = modelId;
  NAME    = name;
  $.post("ajax/fifo/showLocationModel.php",{modelId:modelId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function removeModel(groupModelsId,modelId,name){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      //alert(groupModelsId);
      $.post("ajax/models/delModel.php",{modelId:modelId})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPageModel(groupModelsId,name);
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/fifo/manageFiFo.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        //alert(data.groupModelsId+"<<>>"+data.name);
        showPage();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

$('#formAdd2').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd2').smkValidate()) {
    $.ajax({
        url: 'ajax/fifo/manageFi.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd2').smkClear();
        //alert(data.groupModelsId+"<<>>"+data.name);
        showFormModel(MODELID,NAME);
        MODELID = "";
        NAME    = "";
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal2').modal('toggle');
      }, 1000);
    });
  }
});
