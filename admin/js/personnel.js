function randomPass(){
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!&#@";
  var pass = "";
  var maxs = 10;
  for(var x=0; x<=maxs; x++){
    var i = Math.floor(Math.random()*chars.length);
    pass += chars.charAt(i);
  }
  return pass;
}

function modelCameraClose(){
  $('#modal-camera').modal('hide');
}

function takePhoto(id,type){
  //var windowoption = "width=800,height=500,menubar=0,resizable=0,scrollbars=0,location=0,status=0,titlebar=0,toolbar=0";
  var parm  = "id="+btoa(id)+"&type="+btoa(type);
  var url     = "takePhoto.php?"+ parm;
  //var name = "takePhoto";
  //window.open(url, name, windowoption);
  window.location.replace(url);
}

function showForm(value=""){
  $.post("ajax/personnel/formPersonnel.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
      if($('#personnel_code').val() == ""){
        genCodeID();
      }

  });
}

function showPage(){
  $.post("ajax/personnel/showPersonnel.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function genCodeID(){
  var position_name = $("#position_id option:selected").text();
  var codeArr = position_name.split(":");
  var code    = codeArr[0].trim();
  $.post("ajax/personnel/genCodeID.php",{position_code:code})
    .done(function( data ) {
      $('#personnel_code').val(data);
  });
}


function generatePass(){
  $('.pass').val(randomPass());
}

function ReadSmartCard(){
  $.post("ajax/personnel/smartCard.php")
    .done(function(data) {
      if(data){
        var result = data[0];
        if(result.STATUS == '1'){
          $('#personnel_name').val(result.NAME_TH);
          $('#personnel_lname').val(result.SURNAME_TH);
          $('#personnel_id_card').val(result.ID_CARD);;
          $('#personnel_birthdate').val(formatDate(result.BIRTHDATE));
          if(result.SEX == 1){
            $('#personnel_sex_m').prop('checked', true);
          }else{
            $('#personnel_sex_f').prop('checked', true);
          }
          $('#personnel_address').val(result.ADDRESS + chkstr(result.MOO) + chkstr(result.SOI) + chkstr(result.ROAD)
           + chkstr(result.TUMBOL)+ chkstr(result.AMPHURE)+ chkstr(result.PROVINCE));
        }else{
          setTimeout(function(){
            $.smkAlert({text: result.message,type: result.status});
          }, 1000);
        }
      }
  });
  setTimeout(function(){
    $.get("ajax/personnel/clearCard.php");
  }, 3000);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function chkstr(str){
  return str !=""? " " + str : "";
}

function removeTable(value){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/personnel/delPersonnel.php",{value:value})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPage();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$("#panel2").smkPanel({
  hide: 'full,remove'
});
showPage();

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    var flag = true;
    if($('#user_password').val() != undefined && !$.smkEqualPass('#user_password', '#user_password_comfirm')) {
      // Code here
      flag = false;
    }
    if(flag){
      $.ajax({
          url: 'ajax/personnel/managePersonnel.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formAdd').smkClear();
          showPage();
          $.smkAlert({text: data.message,type: data.status});
          $('#myModal').modal('toggle');
        }, 1000);
      });
    }
  }
});
