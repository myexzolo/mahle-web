function showForm(){
  $.post("ajax/config/formConfig.php")
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}
function uploadSAP(){
  $.get("ajax/config/updateSAP.php")
    .done(function( data ) {
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        showPage();
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
  });

}

function showPage(){
  $.post("ajax/config/showConfig.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/config/manageConfig.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showPage();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
