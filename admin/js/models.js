function showFormGroup(groupModelsId){
  $.post("ajax/models/formGroupModels.php",{groupModelsId:groupModelsId})
    .done(function( data ) {
      $('#myModalGroup').modal('toggle');
      $('#show-form').html(data);
  });
}

function showFormModel(groupModelsId,modelsId,name){

  $.post("ajax/models/formModels.php",{groupModelsId:groupModelsId,modelsId:modelsId,name:name})
    .done(function( data ) {
      //alert(data);
      $('#myModal').modal('toggle');
      $('#show-form2').html(data);
  });
}

function showPageModel(groupModelsId,name){
  //alert(name);
  var header = "<button class='btn btn-success btn-flat btnBack' onclick='showPageGroup()'><i style='font-size:12px' class='fa fa-mail-reply'></i> Back</button>"
    +"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;Management Group Model : " + name
    +" <button class='btn btn-success btn-flat btnAdd' "
    +" onclick=\"showFormModel('"+ groupModelsId +"','','"+name+"')\">+ ADD MODEL</button>";
  $('#nameHeader').html(header);
  //alert(groupModelsId);
  $.post("ajax/models/showModels.php",{groupModelsId:groupModelsId,name:name})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function showPageGroup(){
  var header = "Management Group Models <button class='btn btn-success btn-flat btnAdd' onclick=\"showFormGroup('')\">+ ADD GROUP</button>";
  $('#nameHeader').html(header);
  $.post("ajax/models/showGroupModels.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function removeModelGroup(groupModelsId){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      //alert(groupModelsId);
      $.post("ajax/models/delGroupModels.php",{groupModelsId:groupModelsId})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPageGroup();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


function removeModel(groupModelsId,modelId,name){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      //alert(groupModelsId);
      $.post("ajax/models/delModel.php",{modelId:modelId})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPageModel(groupModelsId,name);
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function removeTable(value){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/groupClass/delGroupClass.php",{value:value})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPageGroup();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formAddModel').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModel').smkValidate()) {
    $.ajax({
        url: 'ajax/models/manageModel.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAddModel').smkClear();
        //alert(data.groupModelsId+"<<>>"+data.name);
        showPageModel(data.groupModelsId,data.name);
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

$('#formAddGroup').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddGroup').smkValidate()) {
    $.ajax({
        url: 'ajax/models/manageGroupModel.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAddGroup').smkClear();
        showPageGroup();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModalGroup').modal('toggle');
      }, 1000);
    });
  }
});
