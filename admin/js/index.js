var page = 0;
TIMEDISPLAY = 0;
var timeout;
var timeout2;
function showBoard(numPage){
  $.post("../board/ajax/showBoard_index.php",{numPage:numPage})
    .done(function( data ) {
      $('#show-page').html(data);
      numPage = $('#numList').val();
      page += numPage;
      if(numPage >= 15){
        fadeIn();
        timeout = setTimeout(function(){
          fadeOut();
          getConfigBoard();
          timeout2 = setTimeout(function(){
            showBoard(page);
            showLateFi();
            showLateFo();
          }, 5000);
        }, TIMEDISPLAY);
      }else if(numPage > 0){
        fadeIn();
        timeout = setTimeout(function(){
          fadeOut();
          getConfigBoard();
          timeout2 = setTimeout(function(){
            showBoard(page);
            showLateFi();
            showLateFo();
          }, 5000);
        }, TIMEDISPLAY)
      }else if(numPage == 0){
        page = 0;
        getConfigBoard();
        showBoard(page);
        showLateFi();
        showLateFo();
      }
  });
}

function showBoardV2(){
  clearTimeout(timeout);
  clearTimeout(timeout2);
  var page = $('#numList').val();
  if(page >= 15){
    page = 15;
  }else if(page > 0){
    page = 0;
  }else if(page == 0){
    page = 0;
  }
  $.post("../board/ajax/showBoard_index.php",{numPage:page})
    .done(function( data ) {
      $('#show-page').html(data);
      numPage = $('#numList').val();
      page += numPage;
      if(numPage >= 15){
        fadeIn();
        timeout = setTimeout(function(){
          fadeOut();
          setTimeout(function(){
            showBoard(page);
          }, 5000);
        }, TIMEDISPLAY);
      }else if(numPage > 0){
        fadeIn();
        timeout = setTimeout(function(){
          fadeOut();
          setTimeout(function(){
            showBoard(page);
          }, 5000);
        }, TIMEDISPLAY)
      }else if(numPage == 0){
        page = 0;
        showBoard(page);
      }
    });
}

function getConfigBoard(){
  $.get( "../board/ajax/getConfigBoard.php", function( data ) {
    TIMEDISPLAY = (data.time_show * 1000);
    $('#responsibility').html(data.responsibility);
    var dateTime = data.update_date;
    var d = dateTime.split(" ");
    var dateTh = d[0].split("-");
    $('#timeUpdate').html(d[1]);
    $('#dateUpdate').html(dateTh[2] + "/" + dateTh[1] + "/" + dateTh[0]);
  });
}

function showLateFi(){
  $.post("ajax/index/showLocation.php",{type:'FI'})
    .done(function( data ) {
      $('#show-Latest-fi').html(data);
  });
}

function showLateFo(){
  $.post("ajax/index/showLocation.php",{type:'FO'})
    .done(function( data ) {
      $('#show-Latest-fo').html(data);
  });
}

function fadeIn(){
  $('.fadeIn').css('visibility', 'visible').animate({opacity: 1.0}, 3000);
}

function fadeOut(){
  $('.fadeIn').animate({opacity: 0}, 3000);
}
