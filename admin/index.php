<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<style>
#sortable, #sortableCat{ list-style-type: none; margin-top: 10px; padding: 0; width: 100%; }
#sortable li, #sortableCat li { margin-bottom: 10px; padding: 10px; font-size: 1.2em; }
html>body #sortable li ,html>body #sortableCat li  { line-height: 1.2em; }
.ui-state-highlight { height:62px; line-height: 1.2em; }
</style>

<body class="hold-transition skin-blue sidebar-mini" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;">
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <?php //include("inc/head-main.php"); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
            <div class="box-header with-border">
              <h3 class="box-title">FIFO Board Management</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="ibody" style="width:100%">
              <div class="section head">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12" style="font-size:18px">FIFO MANAGEMENT SYSTEM-FINISHED GOODS AREA</div>
                    <div class="col-md-12" >
                      <div class="container-fluid  mx-3">
                        <div class="row">
                          <div class="col-lg-4">
                            <p style="font-size:16px">DATE :
                              <span class="txt-yellow" id="dateUpdate">DD/MM/YYYY</span>
                            </p>
                          </div>
                          <div class="col-lg-4">
                            <p style="font-size:16px">UPDATE TIME :
                              <span class="txt-yellow" id="timeUpdate">HH/MM/SS</span>
                            </p>
                          </div>
                          <div class="col-lg-4">
                            <p style="font-size:16px">RESPONSIBILITY :
                              <span class="txt-blue" id="responsibility">(20 MAX TYPE)</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id= "show-page" style="width:100%;"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 margin">
                <div class="btn-group pull-right" style="margin-right:10px;">
                  <button type="button" class="btn bg-navy" onclick="showBoardV2()"><i class="fa fa-chevron-right"></i></button>
                </div>
              </div>
            </div>
            </div>
        </div>
      </div>
      </div>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-6">
        <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">Latest FI</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div id="show-Latest-fi" style="width:100%;"></div>
          </div>
        </div>
    </div>
    <div class="col-md-6">
      <div class="box  boxBlack">
        <div class="box-header with-border">
          <h3 class="box-title">Latest FO</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="show-Latest-fo" style="width:100%;"></div>
            </div>
      </div>
    </div>
      <!--  # coding -->
  </div>
  <!-- /.row -->
  </section>
    <!-- /.content -->

  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>
</div>
<!-- ./wrapper -->
<script src="js/index.js"></script>
<script src="../chart.js/Chart.js"></script>
<script>
  $(function () {
    getConfigBoard();
    showBoard(0);
    showLateFi();
    showLateFo();
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
  })
</script>
</body>
</html>
