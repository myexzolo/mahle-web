<?php
include('../../../conf/connect.php');
include('../../../conf/utils.php');
session_start();
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="50px">#</td>
      <td style="text-align: center;" width="10%">Model</td>
      <td style="text-align: center;" width="15%">Logo</td>
      <td style="text-align: center;">Name</td>
      <td style="text-align: center;" width="10%">ลำดับการแสดง</td>
      <td style="text-align: center;" width="10%">สถานะ</td>
      <td style="text-align: center;display:none;" width="60px">View</td>
      <td style="text-align: center;" width="60px">Edit</td>
      <td style="text-align: center;" width="60px">Del</td>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT * FROM  t_group_models ORDER BY seq";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-github btn-sm" onclick="showPageModel(<?= $row['group_model_id']; ?>,'<?= $row['group_model_name']; ?>')"><i class='fa fa-list'></i></button>
      </td>
      <td style="vertical-align:middle"><img clas="scale-down" src="<?= $row['group_model_img']; ?>" onerror="this.onerror='';this.src='../images/image.png'" style="height: 35px;width:auto;"></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['group_model_name']; ?></td>
      <td style="vertical-align:middle"><?= $row['seq']; ?></td>
      <td style="vertical-align:middle"><?= ($row['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td style="vertical-align:middle;display:none;">
        <button type="button" class="btn btn-flat btn-success btn-sm" onclick="showFormGroupView(<?= $row['group_model_id']; ?>)">View</button>
      </td>
      <td style="vertical-align:middle">
        <button type="button" <?= $_SESSION['DISABLE_EDIT'] ?> class="btn btn-flat btn-warning btn-sm" onclick="showFormGroup(<?= $row['group_model_id']; ?>)">Edit</button>
      </td>
      <td style="vertical-align:middle">
        <button type="button" <?= $_SESSION['DISABLE_DEL'] ?> class="btn btn-flat btn-danger btn-sm" onclick="removeModelGroup(<?= $row['group_model_id']; ?>)">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
