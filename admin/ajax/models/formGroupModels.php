<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$group_model_id = "";
$group_model_name = "";
$group_model_det = "";
$group_model_img = "";
$is_active = "";
$seq = 0;

if(!empty($_POST['groupModelsId'])){
  $sql = "SELECT * FROM  t_group_models WHERE group_model_id = '{$_POST['groupModelsId']}'";
  $query = mysqli_query($conn,$sql);
  $row = mysqli_fetch_assoc($query);

  $group_model_id     = $row['group_model_id'];
  $group_model_name   = $row['group_model_name'];
  $group_model_det    = $row['group_model_det'];
  $group_model_img    = $row['group_model_img'];
  $is_active          = $row['is_active'];
  $seq                = $row['seq'];
}
?>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Model GroupName</label>
      <input value="<?= $group_model_name ?>" name="group_model_name" type="text" maxlength="40" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Group Sequence</label>
      <input style="text-align: right;" value="<?= $seq ?>" name="seq" type="number" maxlength="2" class="form-control" placeholder="Sequence" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" required>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>Group Detail</label>
      <input value="<?= $group_model_det ?>" name="group_model_det" type="text" class="form-control" placeholder="Detail">
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>Group Logo</label>
      <input value="" name="group_model_img" type="file" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>
      <img src="<?= $group_model_img ?>" onerror="this.onerror='';this.src='../images/image.png'" style="height: 60px;">
      </label>
    </div>
  </div>
</div>
<input type="hidden" name="group_model_id" value="<?= $group_model_id ?>">
