<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="30px">#</td>
      <td style="text-align: center;">Name</td>
      <td style="text-align: center;" width="8%">No.</td>
      <td style="text-align: center;" width="10%">Material Number</td>
      <td style="text-align: center;" width="10%">Location</td>
      <td style="text-align: center;" width="10%">Stock Max</td>
      <td style="text-align: center;" width="10%">Stock Min</td>
      <td style="text-align: center;" width="10%">Total Stock</td>
      <td style="text-align: center;" width="10%">ลำดับการแสดง</td>
      <td style="text-align: center;" width="9%">สถานะ</td>
      <td style="text-align: center;" width="40px">Edit</td>
      <td style="text-align: center;" width="40px">Del</td>
    </tr>
  </thead>
  <tbody>
<?php
  $groupModelsId = $_POST['groupModelsId'];
  $name = $_POST['name'];
  //echo ">>>" . $groupModelsId;
  $sql = "SELECT * FROM t_models where group_model_id = $groupModelsId ORDER BY seq";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_name']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_number']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['material_number']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['location']; ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_max']); ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_min']); ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_total']); ?></td>
      <td style="vertical-align:middle;"><?= $row['seq']; ?></td>
      <td style="vertical-align:middle"><?= ($row['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-warning btn-sm" onclick="showFormModel(<?= $groupModelsId ?>,<?= $row['model_id']; ?>,'<?= $name ?>')">Edit</button>
      </td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-danger btn-sm" onclick="removeModel(<?= $groupModelsId ?>,<?= $row['model_id']; ?>,'<?= $name ?>')">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
