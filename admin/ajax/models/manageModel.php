<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
session_start();

$model_id           = $_POST['model_id'];
$model_name         = $_POST['model_name'];
$model_number       = $_POST['model_number'];
$stock_min          = $_POST['stock_min'];
$stock_max          = $_POST['stock_max'];
$stock_total        = $_POST['stock_total'];
$remark             = $_POST['remark'];
$material_number    = $_POST['material_number'];
$location           = $_POST['location'];
$seq                = isset($_POST['seq'])?$_POST['seq']:1;
$is_active          = $_POST['is_active'];
$groupModelsId      = $_POST['groupModelsId'];

$user_id_update = $_SESSION['user_id'];

if(!empty($model_id)){
  $sql = "UPDATE t_models SET
          model_name      = '$model_name',
          model_number    = '$model_number',
          material_number = '$material_number',
          location        = '$location',
          stock_min       = '$stock_min',
          stock_max       = '$stock_max',
          stock_total     = '$stock_total',
          remark          = '$remark',
          is_active       = '$is_active',
          seq             = '$seq',
          update_by       = '$user_id_update'
          WHERE model_id  = '$model_id'";
}else{
  $sql = "INSERT INTO t_models
         (model_name,model_number,material_number,stock_min,stock_max,stock_total,
          group_model_id,remark,is_active,seq,update_by,location)
         VALUES('$model_name','$model_number','$material_number','$stock_min','$stock_max','$stock_total',
          $groupModelsId,'$remark','$is_active','$seq','$user_id_update','$location')";
}

if(mysqli_query($conn,$sql)){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success','groupModelsId' => $groupModelsId ,'name' => $_POST['name'])));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail'.$sql,'groupModelsId' => $groupModelsId ,'name' => $_POST['name'])));
}
?>
