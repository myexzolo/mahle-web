<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$model_id = "";
$model_name = "";
$model_number = "";
$stock_min = 0;
$stock_max = 0;
$stock_total = 0;
$remark = "";
$seq    = 0;
$is_active = "";
$material_number  = "";
$location = "";


//echo "xxxx >>>".$_POST['modelsId'];
if(!empty($_POST['modelsId'])){
  $sql = "SELECT * FROM t_models WHERE model_id = '{$_POST['modelsId']}'";
  $query = mysqli_query($conn,$sql);
  $row = mysqli_fetch_assoc($query);

  $model_id     = $row['model_id'];
  $model_name   = $row['model_name'];
  $model_number = $row['model_number'];
  $stock_min    = $row['stock_min'];
  $stock_max    = $row['stock_max'];
  $stock_total  = $row['stock_total'];
  $material_number  = $row['material_number'];
  $location     = $row['location'];
  $remark       = $row['remark'];
  $seq          = $row['seq'];
  $is_active    = $row['is_active'];
}

?>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Model Name</label>
      <input value="<?= $model_name ?>" name="model_name" type="text" maxlength="20" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Model Number</label>
      <input value="<?= $model_number ?>" name="model_number" type="text" maxlength="15" class="form-control" placeholder="Number" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Stock MIN</label>
      <input style="text-align: right;" value="<?= $stock_min ?>" name="stock_min" type="number" maxlength="10" class="form-control" placeholder="MIN" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Stock MAX</label>
      <input style="text-align: right;" value="<?= $stock_max ?>" name="stock_max" type="number" maxlength="10" class="form-control" placeholder="MAX" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>TOTAL Stock</label>
      <input style="text-align: right;" value="<?= $stock_total ?>" name="stock_total" type="number" maxlength="10" class="form-control" placeholder="Sequence" required>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>Remark</label>
      <input value="<?= $remark ?>" name="remark" type="text" class="form-control" placeholder="Remark">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Material Number</label>
      <input value="<?= $material_number ?>" name="material_number" type="text" maxlength="30" class="form-control" placeholder="Number" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Location</label>
      <input value="<?= $location ?>" name="location" type="text" maxlength="30" class="form-control" placeholder="Number" required>
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Sequence</label>
      <input style="text-align: right;" value="<?= $seq ?>" name="seq" type="number" maxlength="2" class="form-control" placeholder="Sequence" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" required>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
</div>
<input type="hidden" name="model_id" value="<?= $model_id ?>">
<input type="hidden" name="groupModelsId" value="<?= $_POST['groupModelsId'] ?>">
<input type="hidden" name="name" value="<?= $_POST['name'] ?>">
