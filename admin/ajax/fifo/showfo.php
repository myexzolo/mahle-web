<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="30px">#</td>
      <td style="text-align: center;" width="15%">Logo</td>
      <td style="text-align: center;" >Name</td>
      <td style="text-align: center;" >No.</td>
      <td style="text-align: center;" width="20%">Date FO</td>
      <td style="text-align: center;" >Material Storage Location</td>
      <td style="text-align: center;" width="40px">Edit</td>
    </tr>
  </thead>
  <tbody>
<?php
  //echo ">>>" . $groupModelsId;
  $sql = "SELECT f.fifo_id, f.location,f.fo_date, m.model_number,m.model_name,m.material_number, gm.group_model_img FROM t_fifo f,t_models m, t_group_models gm
          where f.model_id = m.model_id and m.group_model_id = gm.group_model_id
          and m.is_active = 'Y' and gm.is_active = 'Y' and f.fifo_type = 'FO'
          ORDER BY f.fo_date DESC";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);

    $fifo_id = $row['fifo_id'];
    $group_model_img = $row['group_model_img'];
    $model_name = $row['model_name'];
    $model_number = $row['model_number'];
    $location = $row['location'];
    $fo_date  = $row['fo_date'];
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle"><img clas="scale-down" src="<?= $row['group_model_img']; ?>" onerror="this.onerror='';this.src='../images/image.png'" style="height: 35px;width:auto;"></td>
      <td style="vertical-align:middle;text-align: left;"><?= $model_name ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $model_number ?></td>
      <td style="vertical-align:middle;text-align: center;"><?= $fo_date ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $location ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-warning btn-sm" onclick="cencelFo('<?= $fifo_id  ?>','<?= $group_model_img ?>','<?= $model_name ?>','<?= $model_number ?>','<?= $location ?>')">Cencel</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
