<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$model_id     = "";
$model_name   = "";
$model_number = "";
$location     = "";
$fifo_id      = "";
$listFi       = "";
$style        = "";

//echo "xxxx >>>".$_POST['modelsId'];
$modelsId   = $_POST['modelsId'];
$type       = $_POST['type'];
$fifoId    = isset($_POST['fifoId'])?$_POST['fifoId']:"";
if($fifoId != ""){
  $sql = "SELECT * FROM t_models m, t_group_models gm, t_fifo f
          where m.group_model_id = gm.group_model_id and m.model_id = $modelsId
          and f.model_id = $modelsId and fifo_id = $fifoId
          and m.is_active = 'Y' and gm.is_active = 'Y'";
}else{
  $sql = "SELECT * FROM t_models m, t_group_models gm
          where m.group_model_id = gm.group_model_id and model_id = $modelsId
          and m.is_active = 'Y' and gm.is_active = 'Y'";
}

$query = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($query);

$model_name   = $row['model_name'];
$model_number = $row['model_number'];
$location     = isset($row['location'])?$row['location']:"";

if($type == "FO"){
  $listFi = getFOLocaltion($modelsId);
  $style  = "style='display:none'";
}
?>
<div class="row">
  <div class="col-md-6" align="center">
    <img clas="scale-down" src="<?= $row['group_model_img']; ?>" onerror="this.onerror='';this.src='../images/image.png'" style="height: 80px;width:auto;">
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Model Name</label>
      <input value="<?= $model_name ?>" readonly name="model_name" type="text" maxlength="20" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Model Number</label>
      <input value="<?= $model_number ?>" readonly name="model_number" type="text" maxlength="15" class="form-control" placeholder="Number" required>
    </div>
  </div>
  <div class="col-md-12" <?=$style ?>>
    <div class="form-group">
      <label>Location</label>
      <input value="<?= $location ?>" id="location" name="location" type="text" maxlength="8" class="form-control" placeholder="Location">
    </div>
  </div>
  <?= $listFi ?>

</div>
<input type="hidden" name="model_id"  value="<?= $modelsId ?>">
<input type="hidden" name="fifo_type" value="<?= $type ?>">
<input type="hidden" id="fifo_id" name="fifo_id"  value="<?= $fifoId ?>">
