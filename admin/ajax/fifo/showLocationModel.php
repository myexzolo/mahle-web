<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="40px">#</td>
      <td style="text-align: center;" width="12%">Name</td>
      <td style="text-align: center;" width="12%">Number</td>
      <td style="text-align: center;" >Material Storage Location</td>
      <td style="text-align: center;" >Date</td>
      <td style="text-align: center;" width="60px">Edit</td>
      <td style="text-align: center;" width="60px">Delete</td>
    </tr>
  </thead>
  <tbody>
<?php
  $modelId = $_POST['modelId'];
  $sql = "SELECT * FROM t_models m, t_fifo f
          where m.model_id = f.model_id and f.fifo_type = 'FI'
          and m.is_active = 'Y' and f.model_id = $modelId
          ORDER BY f.fi_date";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_name']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_number']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['location']; ?></td>
      <td style="vertical-align:middle;text-align: center;"><?= setFormatDate($row['fi_date'],'d/m/Y H:i:s'); ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-warning btn-sm" onclick="showFormFi(<?= $row['fifo_id']; ?>,'<?= $row['location']; ?>')">Edit</button>
      </td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-danger btn-sm" onclick="removeFi(<?= $row['fifo_id']; ?>)">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
