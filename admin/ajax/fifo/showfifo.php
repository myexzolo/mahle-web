<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="30px">#</td>
      <td style="text-align: center;" width="30px">FI</td>
      <td style="text-align: center;" width="30px">FO</td>
      <td style="text-align: center;" width="8%">Logo</td>
      <td style="text-align: center;" width="12%">Name</td>
      <td style="text-align: center;" width="12%">No.</td>
      <td style="text-align: center;" width="8%">Stock Max</td>
      <td style="text-align: center;" width="8%">Stock Min</td>
      <td style="text-align: center;" width="8%">Total Stock</td>
      <td style="text-align: center;" >Material Storage Location</td>
      <td style="text-align: center;" width="40px">Edit</td>
    </tr>
  </thead>
  <tbody>
<?php
  //echo ">>>" . $groupModelsId;
  $sql = "SELECT * FROM t_models m, t_group_models gm
          where m.group_model_id = gm.group_model_id
          and m.is_active = 'Y' and gm.is_active = 'Y'
          ORDER BY gm.group_model_id,m.seq";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle;">
        <button type="button" class="btn btn-flat btn-primary btn-sm" onclick="showForm(<?= $row['model_id']; ?>,'FI')"><i class="fa fa-sign-in"></i></button>
      </td>
      <td style="vertical-align:middle;">
        <button type="button" class="btn btn-flat btn-success btn-sm" onclick="showForm(<?= $row['model_id']; ?>,'FO')"><i class="fa fa-sign-out"></i></button>
      </td>
      <td style="vertical-align:middle"><img clas="scale-down" src="<?= $row['group_model_img']; ?>" onerror="this.onerror='';this.src='../images/image.png'" style="height: 35px;width:auto;"></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_name']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['model_number']; ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_max']); ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_min']); ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= number_format($row['stock_total']); ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= getModelLocaltion($row['model_id']); ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-warning btn-sm" onclick="showFormModel(<?= $row['model_id']; ?>,'<?= $row['model_name']; ?>')">Edit</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
