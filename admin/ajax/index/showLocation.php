<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="ibody">
<table style="width:100%;" align="center">
  <tr class="tr1 table-head" style="border-bottom: 3px #030303 solid;">
    <td class="thboard21" style="width:10%;">S.NO</td>
    <td class="thboard21" >MODEL</td>
    <td class="thboard22" style="width:15%;">LOCATION</td>
  </tr>
  <tbody>
<?php

if(isset($_POST['type'])){

  $type = $_POST['type'];

  $dateUp = "";
  if($type == "FO"){
    $dateUp = "fo_date";
  }else{
    $dateUp = "fi_date";
  }

  $sql = "SELECT * FROM t_models m, t_group_models gm, t_fifo f
          where m.group_model_id = gm.group_model_id and m.model_id = f.model_id
          and f.fifo_type = '$type' and m.is_active = 'Y' and gm.is_active = 'Y'
          ORDER BY $dateUp DESC limit 0,10";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  $classTR = "";
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $fifo_id          = $row['fifo_id'];
    $model_id         = $row['model_id'];
    $location         = $row['location'];
    $model_name       = $row['model_name'];
    $model_number     = $row['model_number'];
    $group_model_img  = isset($row['group_model_img'])?$row['group_model_img']:"";

    if($classTR == "tr0"){
      $classTR = "tr1";
    }else{
      $classTR = "tr0";
    }
    $logo = "";
    if($group_model_img != ""){
      $logo = "<img src='$group_model_img' style='width:auto;height:3.5vh;'>";
    }
    ?>
    <tr class='<?= $classTR ?>'>
      <td class="tbboardF"><div class="fadeIn"><?= $i ?></div></td>
      <td class="tbboardF">
        <div class="p-0 datalist fadeIn">
          <div class="col-md-3" style="float: left;"><?= $logo ?></div>
          <div class="col-md-4" style="float: left; text-align: left; padding-left: 20px;"><?= $model_name ?></div>
          <div class="col-md-5" style="float: left; text-align: left;"><?= $model_number ?></div>
        </div>
      </td>
      <td style="text-align:left;padding-left:10px;" class="tbboardF2"><div class="fadeIn"><?= $location ?></div></td>
    </tr>
<?php
  }
}
?>
</tbody>
</table>
</div>
