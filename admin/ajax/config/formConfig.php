<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$sql = "SELECT * FROM t_config";
$query = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($query);

$time_show      = $row['time_show'];
$time_upload    = $row['time_upload'];
$responsibility = $row['responsibility'];

?>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Responsibility</label>
      <input value="<?= $responsibility ?>" name="responsibility" type="text" maxlength="30" class="form-control" placeholder="Responsibility" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Time Display/Second</label>
      <input style="text-align: right;" value="<?= $time_show ?>" name="time_show" type="number" maxlength="3" class="form-control" placeholder="Time" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Time Upload SAP/minute</label>
      <input style="text-align: right;" value="<?= $time_upload ?>" name="time_upload" type="number" maxlength="3" class="form-control" placeholder="Time" required>
    </div>
  </div>
</div>
