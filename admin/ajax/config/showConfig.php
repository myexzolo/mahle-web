<?php

include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td style="text-align: center;" width="40px">#</td>
      <td style="text-align: center;" width="15%">เวลาในการแสดง/วินาที</td>
      <td style="text-align: center;" width="15%">เวลาในการ Upload SAP/นาที</td>
      <td style="text-align: center;" >RESPONSIBILITY</td>
      <td style="text-align: center;" width="150px">Update Time</td>
      <td style="text-align: center;" width="100px">SAP</td>
      <td style="text-align: center;" width="60px">Edit</td>
    </tr>
  </thead>
  <tbody>
<?php
  //echo ">>>" . $groupModelsId;
  $sql = "SELECT * FROM t_config";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td style="vertical-align:middle" align="center"><?= $i ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= $row['time_show']; ?></td>
      <td style="vertical-align:middle;text-align: right;"><?= $row['time_upload']; ?></td>
      <td style="vertical-align:middle;text-align: left;"><?= $row['responsibility']; ?></td>
      <td style="vertical-align:middle;text-align: center;"><?= setFormatDate($row['update_date'],"d/m/Y H:i:s"); ?></td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-primary btn-sm" onclick="uploadSAP()">Upload</button>
      </td>
      <td style="vertical-align:middle">
        <button type="button" class="btn btn-flat btn-warning btn-sm" onclick="showForm()">Edit</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [ 50, 100, 150,200],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
