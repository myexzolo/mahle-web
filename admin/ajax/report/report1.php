<?php
include('../../../conf/connect.php');
include('../../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$sql = "SELECT * FROM t_models m, t_group_models gm
        where m.group_model_id = gm.group_model_id
        and m.is_active = 'Y' and gm.is_active = 'Y'
        ORDER BY gm.group_model_id,m.seq ";
$query = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);
$numList = $num;
$classTR = "";
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr>
      <th style="width:5%;text-align: center;line-height: 6vh;" rowspan="2">S.NO</th>
      <th style="text-align:center;" colspan="3">MODEL</th>
      <th style="width:12%;text-align: center;"colspan="2">STD STOCK</th>
      <th style="width:12%;text-align:center;line-height: 6vh;" rowspan="2">TOTALSTOCK</th>
      <th style="width:12%;text-align:center;line-height: 6vh;" rowspan="2">QUANTITY</th>
    </tr>
    <tr>
      <th style="width:16%;text-align:center;">Logo</th>
      <th style="text-align:center;">NAME</th>
      <th style="text-align:center;">NUMBER</th>
      <th style="width:10%;text-align:center;">MAX</th>
      <th style="width:10%;text-align:center;">MIN</th>
    </tr>
  </thead>
  <tbody>
<?php
$totalOverMax = 0;
$totallessMin = 0;
for ($i=1; $i <= $num ; $i++) {
  $row = mysqli_fetch_assoc($query);

  $group_model_img  = isset($row['group_model_img'])?$row['group_model_img']:"";
  $model_id         = isset($row['model_id'])?$row['model_id']:"";
  $model_name       = isset($row['model_name'])?$row['model_name']:"";
  $model_number     = isset($row['model_name'])?$row['model_number']:"";
  $stock_min        = isset($row['stock_min'])?$row['stock_min']:"";
  $stock_max        = isset($row['stock_min'])?$row['stock_max']:"";
  $stock_total      = isset($row['stock_min'])?$row['stock_total']:"";

  $logo = "";
  if($group_model_img != ""){
    $logo = "<img src='$group_model_img' style='width:auto;height:3.5vh;'>";
  }
  $listFi = "";
  $quantity = "";
  $textColor = "txt-green";
  if($model_id != ""){

    if($stock_total > $stock_max){
      $textColor = "txt-yellow2";
      $quantity  = number_format($stock_total - $stock_max);
      $totalOverMax += ($stock_total - $stock_max);
    }else if($stock_total < $stock_min){
      $textColor = "txt-red";
      $quantity  = number_format($stock_total - $stock_min);
      $totallessMin += ($stock_total - $stock_min);
    }

    $stock_min = number_format($stock_min);
    $stock_max = number_format($stock_max);
    $stock_total = number_format($stock_total);
  }
?>
<tr>
  <td style="text-align: center;line-height: 3vh;"><?= $i?></td>
  <td style="text-align: center;"><?= $logo ?></td>
  <td style="text-align:left;padding-left:10px;line-height: 3vh;"><?= $model_name ?></td>
  <td style="text-align:left;padding-left:10px;line-height: 3vh;"><?= $model_number ?></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><?= $stock_max ?></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><?= $stock_min ?></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><?= $stock_total ?></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;" class="<?= $textColor ?>"><?= $quantity ?></td>
</tr>
<?php
}
$totalBalance =  ($totalOverMax + $totallessMin);
?>
<tr>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;" colspan="7"><b>Total Over STD STOCK</b></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><b><?= number_format($totalOverMax) ?></b></td>
</tr>
<tr>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;" colspan="7"><b>Total Less STD STOCK</b></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><b><?= number_format($totallessMin) ?></b></td>
</tr>
<tr>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;" colspan="7"><b>Total Balance</b></td>
  <td style="text-align:right;padding-right:10px;line-height: 3vh;"><b><?= number_format($totalBalance) ?></b></td>
</tr>
</tbody>
</table>
