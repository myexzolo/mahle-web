<?php

include('../../../conf/connect.php');

session_start();
$user = $_POST['user'];
$pass = $_POST['pass'];


$pass = md5($pass);

$sql = "SELECT * FROM t_user WHERE user_login ='$user' AND user_password='$pass'";
$query = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);
$row = mysqli_fetch_assoc($query);
if($num == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}else{
  $_SESSION['user_id'] = $row['user_id'];
  $_SESSION['personnelCode'] = $row['user_login'];

  $role_list = isset($row['role_list'])?$row['role_list']:'0';
  if($role_list != '0'){
    $role_list = substr($role_list,1);
  }
  $sql2 = "SELECT * FROM t_role WHERE role_id in ($role_list)";
  $query2 = mysqli_query($conn,$sql2);
  $num2   = mysqli_num_rows($query2);
  $rCode = "";
  $rAccess = "";
  for ($i=0; $i < $num2; $i++){
    $row = mysqli_fetch_assoc($query2);
    if($rCode == ""){
      $rCode = $row['role_code'];
    }else{
      $rCode .= ",".$row['role_code'];
    }

    if($rAccess == ""){
      $rAccess = isset($row['role_access'])?$row['role_access']:"";
    }else{
      $rAccess .= isset($row['role_access'])?",".$row['role_access']:"";
    }
  }
  $_SESSION['ROLEACCESS'] = $rAccess;
  $_SESSION['ROLECODE'] = $rCode;


  $_SESSION['DISABLE_EDIT'] = "";
  $_SESSION['DISABLE_DEL'] = "";

  $posAll   = strrpos($rAccess, "ALL");
  $posED    = strrpos($rAccess, "ED");
  $posE     = strrpos($rAccess, "E");
  $posV     = strrpos($rAccess, "V");

  if ($posAll === false) {
    if($posED === false){
        $_SESSION['DISABLE_DEL']   = "disabled";
      if($posE === false){
        $_SESSION['DISABLE_EDIT']  = "disabled";
      }
    }
  }

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}

?>
