<html>
<head>
   <title>SAPConnection Class: Connection to Application Server</title>
</head>
<body>
<h1>SAPConnection Class: Connection to Application Server</h1>
<?
    include_once ("../sap.php");

    $sap = new SAPConnection();
    // Params:                        hostname   sysnr
    $sap->ConnectToApplicationServer ("jpka-e5jl00.asia1.global.mahle", "00");
    // Params:  client username  password  language
    $sap->Open ("402", "RFC_TH2","Init0001","EN");
    $sap->PrintStatus();
    $sap->GetSystemInfo();
    echo "<BR><PRE>"; print_r ($sap); echo ("</PRE>");
    $sap->Close();
?>
</body>
</html>
